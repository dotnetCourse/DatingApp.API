namespace DatingApp.API.DTOs
{
    public class UserForLoginDto
    {
       
        private string _username;
       
        public string Username { get => _username?.ToLower(); set => _username = value; }
       
        public string Password { get; set; }
    }
}