using System.ComponentModel.DataAnnotations;
namespace DatingApp.API.DTOs
{
    public class UserForRegisterDto
    {
        
        private string _username;
        [Required]
        public string Username { get => _username?.ToLower(); set => _username = value; }
        [Required]
        [StringLength(10, MinimumLength = 4, ErrorMessage="You most specify a password ")]
        public string Password { get; set; }
    }
}